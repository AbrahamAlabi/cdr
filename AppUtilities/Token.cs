﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace HyAdmin.AppUtilities
{
    public class Token
    {
        public string Tokn { get; set; }
        public DateTime? Expiry { get; set; }
        public string Error { get; set; }

        public Token GetToken(string role)
        {
            if (HttpContext.Current.Session["Token"] == null)
            {
                string baseURL = APIServices.BaseURL;
                string endpoint = APIServices.Autentication;

                var client = new RestClient($"{baseURL}{endpoint}");
                var request = new RestRequest(Method.POST);
                
                string client_id = ConfigurationManager.AppSettings[$"hygeia_{role}client_id"];
                string api_secret = ConfigurationManager.AppSettings[$"hygeia_{role}api_secret"];
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded", $"username={client_id}&password={api_secret}&grant_type=password", ParameterType.RequestBody);

                JavaScriptSerializer js = new JavaScriptSerializer();
                IRestResponse response = client.Execute(request);
                bool success = response.IsSuccessful;
                string error = response.ErrorMessage;

                if (!success)
                {
                    return new Token()
                    {
                        Error = error,
                        Expiry = null,
                        Tokn = null
                    };
                }

                dynamic token_response = js.Deserialize<dynamic>(response.Content);
                Token t = new Token
                {
                    Error = null,
                    Expiry = DateTime.Now.AddMinutes(25),
                    Tokn = token_response["access_token"]
                };
                HttpContext.Current.Session["Token"] = t;
                return t;
            }
            else
            {
                Token t = (Token)HttpContext.Current.Session["Token"];
                if (DateTime.Now > t.Expiry)
                {
                    HttpContext.Current.Session.Remove("Token");
                    return (new Token()).GetToken(role);
                }

                return t;
            }
        }
    }
}