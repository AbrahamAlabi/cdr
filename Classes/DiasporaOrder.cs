﻿using HyAdmin.Models.ExistingDBs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace HyAdmin.Classes
{
    public class DiasporaOrder
    {
        public string URL { get; set; } = ConfigurationManager.AppSettings["diaspora_pay_url"];
        public string username { get; set; } = "hygieahmo";
        public string TransactitonReference { get; set; }
        public DiasporaHash Diaspora { get; set; }
        public double Amount { get; set; }
        public string Email { get; set; }


        public DiasporaHash GetAppsh()
        {
            string salt = GetSalt();
            string apiKey = ConfigurationManager.AppSettings["diasporah_api_key"];
            string sha1ofapikey = SHA1Hash(apiKey);
            string sha1ofsaltnapikey = SHA1Hash(salt + sha1ofapikey);
            return new DiasporaHash
            {
                Salt = salt, // random string => length 40
                Hash = sha1ofsaltnapikey, //03459d2cac8b4e7cb6d509024f06d648
            };
        }

        static string SHA1Hash(string input)
        {
            var hash = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Concat(hash.Select(b => b.ToString("x2")));
        }

        static string SHA512Hash(string input)
        {
            using (SHA512 sha1 = new SHA512Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }
        }

        static string GetSalt()
        {
            return (Guid.NewGuid().ToString() + Guid.NewGuid().ToString()).Substring(0, 40);
        }

        string GenerateTxnNo(int RetailCustomerId, int PlanId)
        {
            string trxnNo = "";
            int idlenrem = 7 - RetailCustomerId.ToString().Length;
            #region Hide
            string rand = rand = DateTime.Now.ToString("yymmddssffaa").Replace(":", "").Replace("-", "").Replace("/", "").Replace("0", "").Substring(0, idlenrem);
            switch (PlanId.ToString())
            {
                case "167":
                    #region 167
                    trxnNo = "HYBI" + RetailCustomerId + rand;
                    break;
                #endregion
                case "171":
                    #region 171
                    trxnNo = "HYBI" + RetailCustomerId + rand;
                    break;
                #endregion
                case "168":
                    #region 168
                    trxnNo = "HYBF" + RetailCustomerId + rand;
                    break;
                #endregion
                case "169":
                    #region 169
                    trxnNo = "HYBF" + RetailCustomerId + rand;
                    break;
                #endregion
                //-------------------------------------------------------------
                case "1770":
                    #region 177
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                case "1771":
                    #region 177
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                case "1780":
                    #region 178
                    break;
                #endregion
                case "1781":
                    #region 178
                    trxnNo = "HYST" + RetailCustomerId + rand;
                    break;
                #endregion
                //---------------------------------------------------------------
                case "185":
                    #region 185
                    trxnNo = "SCMN" + RetailCustomerId + rand;
                    break;
                #endregion
                case "184":
                    #region 184
                    trxnNo = "SCMD" + RetailCustomerId + rand;
                    break;
                #endregion
                case "186":
                    #region 186
                    trxnNo = "SCMP" + RetailCustomerId + rand;
                    break;
                #endregion
                case "212":
                    #region 212
                    trxnNo = "BIZS" + RetailCustomerId + rand;
                    break;
                #endregion
                //---------------------------------------------------------------
                case "391":
                    #region 391
                    trxnNo = "CRDO" + RetailCustomerId + rand;
                    break;
                #endregion
                case "392":
                    #region 392
                    trxnNo = "CRDT" + RetailCustomerId + rand;
                    break;
                    #endregion
            }
            #endregion
            return trxnNo;
        }


        public IRestResponse CreateOrder(int RetailCustomerId, int PlanId, string Email)
        {
            TransactitonReference = GenerateTxnNo(RetailCustomerId, PlanId);
            DiasporaHash Diaspora = GetAppsh();
            double Amount = 0;

            using (var db = new CBAProd())
            {
                var gp = db.group_plan.Where(p => p.planid == PlanId && p.iscurrent == true).FirstOrDefault();
                if (gp == null)
                    return null;

                Amount = 100; // (double)(gp.familyprice > 0 ? gp.familyprice : gp.individualprice);
            }

            using(var db = new Models.ExistingDBs.HyDirect())
            {
                var customer = db.RetailCustomers.Find(RetailCustomerId);
                if (customer != null)
                {
                    customer.trxn_ref = TransactitonReference;
                    db.SaveChanges();
                }
            }

            var client = new RestClient(URL);
            string parameters = "{\r\n\t\"username\": \"hygieahmo\",\r\n\t\"salt\" : \"" + Diaspora.Salt + "\",\r\n\t\"apiKey\":\"" + Diaspora.Hash + "\",\r\n\t\"command\": \"createOrder\",\r\n\t\"amount\" : \"" + Amount + "\",\r\n\t\"currencyId\" : \"GBP\",\r\n\t\"customerEmail\": \"" + Email + "\",\r\n\t\"merchantReference\": \"" + TransactitonReference + "\"\r\n}";
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddParameter("undefined", parameters, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }
    }

    public class DiasporaHash
    {
        public string Salt { get; set; }
        public string Hash { get; set; }
    }
}