﻿using HyAdmin.AppUtilities;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    public class CDRController : Controller
    {
        // GET: CDR
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadCSV()
        {
            return View();
        }

        public ActionResult ManageCDR()
        {
            return View();
        }

        public ActionResult Reports()
        {
            return View();
        }

        public ActionResult CDRMembers()
        {
            return View();
        }

        public ActionResult FlaggedItems()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadCDREntry(string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.UploadCDREntry;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCDRProcedures(string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AddCDRProcedures;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCDRProviders(string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AddCDRProviders;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignCDRProcedures(string LegacyCode, int DependantId, string ProcedureCode, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AssignCDRProcedures;
            var client = new RestClient($"{baseURL}{endpoint}/{LegacyCode}/{DependantId}/{ProcedureCode}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"LegacyCode={LegacyCode}&DependantId={DependantId}&ProcedureCode={ProcedureCode}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AssignCDRProviders(string LegacyCode, int DependantId, int ProviderId, string __RequestVerificationToken)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AssignCDRProviders;

            var client = new RestClient($"{baseURL}{endpoint}/{LegacyCode}/{DependantId}/{ProviderId}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"LegacyCode={LegacyCode}&DependantId={DependantId}&Procedure={ProviderId}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
    }
}