﻿using HyDirect.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,Staff")]
    public class DashboardController : Controller
    {
        public ActionResult CredEquity()
        {
            return View();
        }
    }
}