﻿using HyDirect.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,User,MerchantAdmin,Staff")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CV()
        {
            return View();
        }
    }
}