﻿using HyAdmin.AppUtilities;
using HyAdmin.Classes;
using HyAdmin.HDS;
using HyAdmin.Models.ViewModels;
using HyAdmin.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace HyAdmin.Controllers
{
    public class ProductsController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Setup()
        {
            return View();
        }

        [Authorize]
        public ActionResult Edit()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Payment(int Id, string guid)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.LocalRetailCustomer;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}/{guid}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic result = js.Deserialize<dynamic>(response.Content);
            RetailCustomerInfo r = new RetailCustomerInfo();
            if (response.IsSuccessful)
            {
                r.Id = Id;
                r.Email = result["email"];
                r.Phone = result["phone"];
                r.Guid = result["guid"];
                r.Date = Convert.ToDateTime(result["dateadded"].ToString()).ToString("dd MMMM, yyyy");
                r.Name = $"{result["lastname"]} {result["firstname"]} {result["othername"]}";
                r.PlanId = Convert.ToInt32(result["planid"]);
                try { r.MemberId = Convert.ToInt32(result["memberid"]); } catch { }

                r.sResponse = result["resp"];
                r.TransferConfirmed = Convert.ToBoolean(result["confirmed"]);
                
                PlanDetails p = new PlanDetails();
                var x = p.GetPlanDefaults(r.PlanId);
                r.PlanName = x.PlanName;
                r.TransactionReference = x.GenerateTxnNo(Id, r.PlanId);
                
                // Amoounts & Convenience
                const double INTERSWITCH_CAPP = 2000;
                const double INTERSWITCH_PERCENTAGE = 0.985;
                r.PlanPrice = ((x.PlanName.ToLower().Contains("family")) ? x.FamilyPrice : x.IndividualPrice);
                r.Amount = r.PlanPrice / INTERSWITCH_PERCENTAGE;
                r.Convenience = r.Amount - r.PlanPrice;
                if (r.Convenience > INTERSWITCH_CAPP)
                    r.Amount = (r.PlanPrice + INTERSWITCH_CAPP);
                r.Convenience = r.Amount - r.PlanPrice;
                
                r.ProductId = OnlinePayment.ProductId;
                r.ItemId = OnlinePayment.PayItemId;
                r.Redirect = OnlinePayment.RedirectURL;
                r.MacKey = OnlinePayment.MacKey;
                r.HashString = $"{r.TransactionReference}{OnlinePayment.ProductId}{OnlinePayment.PayItemId}{Convert.ToDouble(Convert.ToDecimal(r.Amount).ToString("0.##")) * 100}{r.Redirect}{OnlinePayment.MacKey}";

                #region Save Transaction Reference Number
                client = new RestClient($"{baseURL}{APIServices.SaveTransactionReference}/{Id}/{guid}/{r.TransactionReference}");
                request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Bearer " + t.Tokn);
                request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
                response = client.Execute(request); 
                #endregion
            }
            else
            {
                r.Error = "Network connection failed. Please try again later";
                IntelService s = new IntelService();
                s.SendMail("dbello@hygeiahmo.com,aadeyanju@hygeiahmo.com", "Product Purchase Failed", 
                    $"Dear Both,<br /><br />It looks like a network error occured for customer with Id ({Id}).<br />" +
                    $"You may want to engage the customer.<br /><br />Best Regards,", "", "", 1);
            }
            Session["r"] = r;
            return View(r);
        }

        [AllowAnonymous]
        public ActionResult TPayment(int Id, string email)
        {
            ViewBag.Id = Id;
            ViewBag.email = email;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Diaspora(int Id)
        {

            ViewBag.Id = Id;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public string CO()
        {
            try
            {
                DiasporaOrder d = new DiasporaOrder();
                DiasporaHash Diaspora = d.GetAppsh();
                var client = new RestClient("https://test.diasporapay.net/api.php");
                string parameters = "{\r\n\t\"username\": \"hygieahmo\",\r\n\t\"salt\" : \"" + Diaspora.Salt + "\",\r\n\t\"apiKey\":\"" + Diaspora.Hash + "\",\r\n\t\"command\": \"createOrder\",\r\n\t\"amount\" : \"100\",\r\n\t\"currencyId\" : \"GBP\",\r\n\t\"customerEmail\": \"odm.major@gmail.com\",\r\n\t\"merchantReference\": \"" + Guid.NewGuid().ToString() + "\"\r\n}";
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddParameter("undefined", parameters, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                return response.Content.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public string CreateOrder(int retailCustomerId, string __RequestVerificationToken)
        {
            using (var db = new Models.ExistingDBs.HyDirect())
            {
                var retailC = db.RetailCustomers.Find(retailCustomerId);
                if (retailC != null)
                {
                    DiasporaOrder d = new DiasporaOrder();
                    var OrderResponse = d.CreateOrder(Convert.ToInt32(retailCustomerId), retailC.planid, retailC.email);
                    return OrderResponse.Content.ToString();
                }
                return "Retail customer not found";
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult PaymentDetails()
        {
            RetailCustomerInfo r = new RetailCustomerInfo();
            try
            {
                r = (RetailCustomerInfo)Session["r"];
                Session.Abandon();
            }
            catch { r.Error = "Unable to get details. Please check your internet and refresh your browser"; };
            return Json(r, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult AcceptTerms(int Id)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AcceptTerms;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.IsSuccessful), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RejectTerms(int Id)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RejectTerms;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.IsSuccessful), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdatePaymentType(int Id, int PaymentMethod)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.UpdatePaymentType;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}/{PaymentMethod}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json((response.IsSuccessful), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult EmailTransferDetails(string name, string email, string amount, string transactionReference)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.EmailTransferDetails;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn); request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"Name={name}&Email={email}&Amount={amount}&TransactionReference={transactionReference}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SavePaymentDetails(string amount, string desc, string payRef, string resp, string retRef, string trxn_ref, int customerId, string UniqueId)
        {
            RetailCustomerInfo r = new RetailCustomerInfo();
      
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SavePaymentDetails;

            var client = new RestClient($"{baseURL}{endpoint}/{customerId}/{UniqueId}/{amount}/{desc}/{payRef}/{resp}/{((String.IsNullOrEmpty(retRef)) ? "-" : retRef)}/{trxn_ref}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.IsSuccessful, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdatePaymentDetails(string amount, string resp, string desc, int customerId, string UniqueId, string transactionDate)
        {
            RetailCustomerInfo r = new RetailCustomerInfo();

            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.UpdatePaymentDetails;

            var client = new RestClient($"{baseURL}{endpoint}/{customerId}/{UniqueId}/{amount}/{resp}/{desc}/{transactionDate.Replace(":", ",")}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            return Json(response.IsSuccessful, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult PaymentComplete(int Id, string guid)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.LocalRetailCustomer;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}/{guid}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic result = js.Deserialize<dynamic>(response.Content);
            RetailCustomerInfo r = new RetailCustomerInfo();
            if (response.IsSuccessful)
            {
                r.Id = Id;
                r.Email = result["email"];
                r.Phone = result["phone"];
                r.Guid = result["guid"];
                r.Date = Convert.ToDateTime(result["dateadded"].ToString()).ToString("dd MMMM, yyyy");
                r.LastName = result["lastname"];
                r.FirstName = result["firstname"];
                r.OtherName = result["othername"];
                r.PlanId = Convert.ToInt32(result["planid"]);

                try { r.PaymentMethod = Convert.ToInt32(result["paymentmethod"]); } catch { }
                r.sDescription = result["descr"];
                r.sPaymentReference = result["payRef"];
                r.sResponse = result["resp"];
                try { r.AmountSent = Convert.ToDouble(result["amountsent"]); } catch { r.AmountSent = 0; }
                r.TransferConfirmed = Convert.ToBoolean(result["confirmed"]);
                try { r.MemberId = Convert.ToInt32(result["memberid"]); } catch { }
                try { r.RegistrationDate = Convert.ToDateTime(result["registrationdate"].ToString()).ToString("yyyy-MM-dd"); } catch { }


                //r.ProductId = OnlinePayment.ProductId;
                //r.MacKey = OnlinePayment.MacKey;

                PlanDetails p = new PlanDetails();
                var x = p.GetPlanDefaults(r.PlanId);
                r.PlanName = x.PlanName;
                r.TransactionReference = result["transactionReference"];

                // Amoounts & Convenience
                const double INTERSWITCH_CAPP = 2000;
                const double INTERSWITCH_PERCENTAGE = 0.985;
                r.PlanPrice = ((x.PlanName.ToLower().Contains("family")) ? x.FamilyPrice : x.IndividualPrice);
                r.Amount = r.PlanPrice / INTERSWITCH_PERCENTAGE;
                r.Convenience = r.Amount - r.PlanPrice;
                if (r.Convenience > INTERSWITCH_CAPP)
                    r.Amount = (r.PlanPrice + INTERSWITCH_CAPP);
                r.Convenience = r.Amount - r.PlanPrice;
                
               
                //double amount = Convert.ToDouble(Convert.ToDecimal(r.Amount).ToString("0.##")) * 100;
                ////d.ProductId + d.TransactionReference + d.MacKey;
                //string hash = GenerateSHA512String($"{OnlinePayment.ProductId}{r.TransactionReference}{OnlinePayment.MacKey}");
                //client = new RestClient($"https://webpay.interswitchng.com/collections/api/v1/gettransaction.json?productid={OnlinePayment.ProductId}&transactionreference={r.TransactionReference}&amount={amount}");
                //request = new RestRequest(Method.GET);
                //request.AddHeader("Hash", hash);
                //response = client.Execute(request);
                //try
                //{
                //    dynamic rx = js.Deserialize<dynamic>(response.Content);
                //    r.RequeryResponse = $"{rx["Amount"]},{rx["MerchantReference"]},{rx["ResponseCode"]},{rx["ResponseDescription"]}";
                //}
                //catch { }
                
            }
            else
            {
                r.Error = "Network connection failed. Please try again later";
                IntelService s = new IntelService();
                s.SendMail("dbello@hygeiahmo.com,aadeyanju@hygeiahmo.com", "Product Purchase Failed",
                    $"Dear Both,<br /><br />It looks like a network error occured for customer with Id ({Id}) on Products/PaymentComplete Action.<br />" +
                    $"You may want to engage the customer.<br /><br />Best Regards,", "", "", 1);
            }
            Session["r"] = r;
            return View(r);
        }

        public static string GenerateSHA512String(string inputString)
        {
            using (SHA512 sha512 = SHA512Managed.Create())
            {
                byte[] bytes = Encoding.UTF8.GetBytes(inputString);
                byte[] hash = sha512.ComputeHash(bytes);
                return GetStringFromHash(hash);
            }
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult ReQuery(string transactionreference, double amount, string hash)
        {
            string url = "";
            try
            {
                //url = ConfigurationManager.AppSettings["InterswitchReQueryURL"];

                //int ProductId = OnlinePayment.ProductId;
                //int ItemId = OnlinePayment.PayItemId;

                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                //var myRequest = WebRequest.CreateHttp(url + "?productid=" + ProductId + "&transactionreference=" + transactionreference + "&amount=" + amount);
                //myRequest.Method = "GET";
                //myRequest.Headers.Add("hash", hash);
                ////myRequest.Host = "localhost:22618";
                //myRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; MS Web Services Client Protocol 4.0.30319.239)";

                //using (var theResponse = myRequest.GetResponse())
                //{
                //    var dataStream = theResponse.GetResponseStream();
                //    StreamReader reader = new StreamReader(dataStream);
                //    object objResponse = reader.ReadToEnd();
                //    dataStream.Close();
                //    theResponse.Close();
                //    return Json(objResponse.ToString(), JsonRequestBehavior.AllowGet);
                //}

                var client = new RestClient($"https://webpay.interswitchng.com/collections/api/v1/gettransaction.json?productid=2044&transactionreference={transactionreference}&amount={amount}");
                var request = new RestRequest(Method.GET);
                //request.AddHeader("Connection", "keep-alive");
                //request.AddHeader("Keep-Alive", "300");
                //request.AddHeader("Accept-Language", "en-us,en;q=0.5");
                //request.AddHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1");
                //request.AddHeader("Host", "webpay.interswitchng.com");
                request.AddHeader("Hash", hash);
                //request.AddHeader("Hash", "1b1c501b15a112dfc65938d27758d8d550b1ef23ba6fa8c41894ca7dc8d498ed6cb93a45d6eefd1824f11f74feb977927f7fec780d0b00e146bc194b5e057949");
                IRestResponse response = client.Execute(request);
                return Json(response.Content, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { URL = url, Error = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult LocalRetailCustomerPaymentComplete(int Id)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.LocalRetailCustomerPaymentComplete;

            var client = new RestClient($"{baseURL}{endpoint}/{Id}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic result = js.Deserialize<dynamic>(response.Content);
            RetailCustomerInfo r = new RetailCustomerInfo();
            if (response.IsSuccessful)
            {
                r.Id = Convert.ToInt32(Id);
                r.Email = result["email"];
                r.Phone = result["phone"];
                r.Name = $"{result["lastname"]}{result["firstname"]}";
                //=====================================================
                r.MemberId = -1;
                r.LegacyCode = "";
                r.DependantId = -1;
                r.PaymentApproved = Convert.ToBoolean(result["paymentApproved"]);
                r.Message = "";
                //=====================================================
                r.PlanId = Convert.ToInt32(result["planid"]);
                PlanDetails p = new PlanDetails();
                var x = p.GetPlanDefaults(r.PlanId);
                r.PlanName = x.PlanName;
                r.TransactionReference = result["transactionReference"];
                r.Amount = ((x.PlanName.ToLower().Contains("family")) ? x.FamilyPrice : x.IndividualPrice);
            }
            else
            {
                r.Error = "Network connection failed. Please try again later";
                IntelService s = new IntelService();
                s.SendMail("dbello@hygeiahmo.com,aadeyanju@hygeiahmo.com", "Product Purchase Failed #Error22",
                    $"Dear Both,<br /><br />It looks like a network error occured for customer with Id ({Id}).<br />" +
                    $"You may want to engage the customer.<br /><br />Best Regards,", "", "", 1);
            }
            return Json(r, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public bool UploadCustomerImage(string imageData, string destination)
        {
            try
            {
                string fileNameWithPath = $"{HostingEnvironment.ApplicationPhysicalPath}\\Uploads\\Registration\\Images\\{destination}";
                using (FileStream fs = new FileStream(fileNameWithPath, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        byte[] data = Convert.FromBase64String(imageData);
                        bw.Write(data);
                        bw.Close();
                        return true;
                    }
                }
            }
            catch {
                return false;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult UpdateCustomerData(int CustomerId, string Address, int Country, int City, int IDTypeId, string IDNo, int MaritalStatus, int Gender, string Guid)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.UpdateCustomerDetails;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"CustomerId={CustomerId}&Address={Address}&Country={Country}&City={City}&IDTypeId={IDTypeId}&IDNo={IDNo}&MaritalStatus={MaritalStatus}&Gender={Gender}&Guid={Guid}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            
            string AppURL = (ConfigurationManager.AppSettings["env"] == "development") ? ConfigurationManager.AppSettings["PROJECT_ROOT_LOCAL"] : ConfigurationManager.AppSettings["PROJECT_ROOT_LIVE"];
            string Image = $"{AppURL}Uploads/Registration/Images/{CustomerId}.png";

            if (response.IsSuccessful)
            {

                JavaScriptSerializer js = new JavaScriptSerializer();
                dynamic APIResp = js.Deserialize<dynamic>(response.Content);

                string PasswordSelectionURL = $"{AppURL}Account/PasswordSelection/{CustomerId}/?guid={Guid}";
                endpoint = APIServices.RegisterRetailEnrollee;
                client = new RestClient($"{baseURL}{endpoint}");
                request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("authorization", "Bearer " + t.Tokn);
                request.AddParameter("application/x-www-form-urlencoded", $"Onboard=true&Dob={APIResp["dob"]}&Email={APIResp["email"]}&PlanId={APIResp["planid"]}&Phone={APIResp["phone"]}&OtherName={APIResp["othername"]}&FirstName={APIResp["firstname"]}&LastName={APIResp["lastname"]}&Address={Address}&Gender={Gender}&MaritalStatusId={MaritalStatus}&CoverageClass=2&Image={Image}&IDTypeId={IDTypeId}&IDNo={IDNo}&TitleId={((Gender == 1) ? 1 : 5)}&PasswordSelectionURL={PasswordSelectionURL}&RetailCustomerId={CustomerId}", ParameterType.RequestBody);
                response = client.Execute(request);
                return Json(new { Success = response.IsSuccessful, Email = APIResp["email"], Response = response.Content });
            }

            return Json(new { Success = false, Message = response.Content });
        }
        
        [AllowAnonymous]
        public ActionResult LoadCities(string StateId)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.LoadCities;

            var client = new RestClient($"{baseURL}{endpoint}/{StateId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
       
            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

    }
}