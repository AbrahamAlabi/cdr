﻿using HyAdmin.AppUtilities;
using HyDirect.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,MerchantAdmin,Staff")]
    public class ProjectsController : Controller
    {
        // GET: Projects/Index
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ExistingProjects()
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ExistingProjects;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }
        

    }
}