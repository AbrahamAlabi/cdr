﻿using HyAdmin.AppUtilities;
using HyAdmin.HDS;
using HyAdmin.Services;
using HyDirect.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [Authorize]
    [MaintenanceFilter]
    public class SalesController : Controller
    {
        // GET: Sales
        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult ConfirmPayments()
        {
            return View();
        }
        
        public ActionResult Registered()
        {
            return View();
        }

        public ActionResult CaptureNewMS()
        {
            return View();
        }

        public ActionResult GetPlans()
        {
            var plans = (new PlanDetails()).GetAllplans();
            return Json(plans, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPlanPrice(int PlanId)
        {
            PlanDetails planDetails = new PlanDetails();
            planDetails = planDetails.GetPlanDefaults(PlanId);
            return Json(new { Price = (planDetails.IndividualPrice == 0) ? planDetails.FamilyPrice : planDetails.IndividualPrice });

        }

        [HttpGet]
        public ActionResult IDCPRegistration()
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.IDCPRegistration;
            
            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[Authorize(Roles = "MerchantAdmin")]
        //[ValidateAntiForgeryToken]
        public ActionResult CaptureNew (string PurchaseDate, string AmountPaid, string PlanId, string ActivationCode, string Email, string FirstName, string LastName, string OtherName, string Phone)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.CaptureMerchantRegistration;

            string Adjuster = (User.Identity.Name.Contains("@hygeiahmo.com")) ? User.Identity.Name : User.Identity.Name + "@hygeiahmo.com";

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded",
                $"PurchaseDate={DateTime.Parse(PurchaseDate)}&" +
                $"AmountPaid={AmountPaid}&" +
                $"PlanID={PlanId}&" +
                $"Email={Email}&" +
                $"FirstName={FirstName}&" +
                $"LastName={LastName}&" +
                $"OtherName={OtherName}&" +
                $"Phone={Phone}&" +
                $"Adjuster={Adjuster}",
                ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendRegistrationMail(string activationCode, int PlanId, string firstname, string lastname, string email, string phone, string planname, double amountPaid)
        {
            string hydirectV3Root = Util.HyDirectV3;
            string URL = $"{hydirectV3Root}Registration/LoadForm?pid={PlanId}";
            string name = $"{firstname} {lastname}";

            Util.SendActivationCodeBySMS(activationCode, phone, URL);
            string recRes = Util.SendReceipt(name, "", email, planname, amountPaid, 0.00);
            string actRes = Util.SendActivationCodeByMail(activationCode, URL, name, email);

            IntelService s = new IntelService();
            string merchantAdminEmail = ConfigurationManager.AppSettings["merchantAdminEmail"];
            string message = "Dear User,<br /><br />Above subject refers;<br />This is to notifiy all parties of a merchant sale with details below:<br><br>" +
                $"<strong>Name:</strong> {lastname} {firstname}<br>" +
                $"<strong>Email:</strong> {email}<br>" +
                $"<strong>Phone:</strong> {phone}<br>" +
                $"<strong>Plan:</strong> {planname}<br>" +
                $"<strong>Amount Paid:</strong> {amountPaid}<br>" +
                $"<strong>Date:</strong> {DateTime.Now}<br><br>Regards,";
            string user = (User.Identity.Name.Contains("@hygeiahmo.com")) ? User.Identity.Name : User.Identity.Name + "@hygeiahmo.com";
            s.SendMail(merchantAdminEmail, "Merchant Registration Data Capture Notification", message, "", "", 1);

            return Json(new { Success = (recRes == "success" && actRes == "success") }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Invalidate(int id)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.InvalidateMerchantSales;

            var client = new RestClient($"{baseURL}{endpoint}/{id}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.DeleteMerchantSales;

            var client = new RestClient($"{baseURL}{endpoint}/{id}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendMail()
        {
            IntelService s = new IntelService();
            return Content(s.SendMail("dbello@hygeiahmo.com", "Test", "Message", "odm.major@gmail.com", "", 1));
        }

        //[Authorize(Roles = "MerchantAdmin")]
        public ActionResult MerchantSales()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AllSales(string start, string end)
        {
            Token t = new Token();
            t = t.GetToken("");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.AllSales;

            var client = new RestClient($"{baseURL}{endpoint}/{start}/{end}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

            return Json(response.Content, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult TestImageUpload(string source)
        {
            Token t = new Token();
            t = t.GetToken("");

            var client = new RestClient($"http://online.hygeiahmo.com/hygeiaapiservice/api/Registration/SavePictureOnline?Source={source}");
            var request = new RestRequest(Method.POST);
            //request.AddHeader("postman-token", "ee7e00a5-e396-40b4-27e5-bc052f597736");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            IRestResponse response = client.Execute(request); ;

            return Json(response.Content);
        }

    }
}