﻿using HyAdmin.AppUtilities;
using HyDirect.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,MerchantAdmin,Staff")]
    public class TPSManagementController : Controller
    {
        // GET: TPSManagement
        public ActionResult Index()
        {
            // This should be the dashboard containing statistics
            // -- No of subscriptions, subscription rates, Turn Over/Conversion rate, Average Monthly Registrations,
            // -- Total enrollees registered, Total enrolleess suspended, termination, total pending renewals (Cost, Count)
            // There should be moratorium on these group of plans
            return View();
        }

        public ActionResult Requests()
        {
            return View();
        }

        public ActionResult RequestsData()
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.TPSManagementData;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Respond(string __RequestVerificationToken, int id, string category, bool isApproved, string comments)
        {
            Token t = new Token();
            t = t.GetToken("admin");
            string baseURL = APIServices.BaseURL;

            string endpoint = "";
            string adjuster = User.Identity.Name.Replace("@hygeiahmo.com", "");

            switch (category)
            {
                case "Activation Requests":
                    endpoint = APIServices.TPSRegistration;
                    break;
                case "Re-Activation Requests":
                    endpoint = APIServices.TPSReActivation;
                    break;
                case "Suspension Requests":
                    endpoint = APIServices.TPSSusension;
                    break;
                case "Termination Requests":
                    endpoint = APIServices.TPSTermination;
                    break;
            }

            var request = new RestRequest(Method.POST);
            var client = new RestClient($"{baseURL}{endpoint}");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"Id={id}&Comments={comments}&Adjuster={adjuster}&Response={isApproved}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
            
        }
    }
}