﻿using HyAdmin.AppUtilities;
using HyDirect.Filters;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyAdmin.Controllers
{
    [MaintenanceFilter]
    [Authorize(Roles = "Admin,MerchantAdmin,Staff")]
    public class TimePeriodController : Controller
    {
        // GET: TPSManagement
        public ActionResult Index()
        {
            // This should be the dashboard containing statistics
            // -- No of subscriptions, subscription rates, Turn Over/Conversion rate, Average Monthly Registrations,
            // -- Total enrollees registered, Total enrolleess suspended, termination, total pending renewals (Cost, Count)
            // There should be moratorium on these group of plans
            return View();
        }
        
        public ActionResult SetupBenefits()
        {
            return View();
        }

        public ActionResult SetupMonetaryLimits()
        {
            return View();
        }
        
        public ActionResult Requests()
        {
            return View();
        }

        public ActionResult RetailGraduatedPlanTPeriods()
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.RetailGraduatedPlanTPeriods;

            var client = new RestClient($"{baseURL}{endpoint}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PlanBenefits(int id)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.PlanBenefits;

            var client = new RestClient($"{baseURL}{endpoint}/{id}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        [Route("TimePeriod/BenefitProcedures/{benefitCodeId}/{planId}/{timePeriodId}")]
        public ActionResult BenefitProcedures(int benefitCodeId, int planId, int timePeriodId)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.BenefitProcedures;

            var client = new RestClient($"{baseURL}{endpoint}/{benefitCodeId}/{planId}/{timePeriodId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BenefitLookUp(int benefitCodeId)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.BenefitLookUp;

            var client = new RestClient($"{baseURL}{endpoint}/{benefitCodeId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Respond(string __RequestVerificationToken, int id, string category, bool isApproved, string comments)
        {
            Token t = new Token();
            t = t.GetToken("admin");
            string baseURL = APIServices.BaseURL;

            string endpoint = "";
            string adjuster = User.Identity.Name.Replace("@hygeiahmo.com", "");

            switch (category)
            {
                case "Activation Requests":
                    endpoint = APIServices.TPSRegistration;
                    break;
                case "Re-Activation Requests":
                    endpoint = APIServices.TPSReActivation;
                    break;
                case "Suspension Requests":
                    endpoint = APIServices.TPSSusension;
                    break;
                case "Termination Requests":
                    endpoint = APIServices.TPSTermination;
                    break;
            }

            var request = new RestRequest(Method.POST);
            var client = new RestClient($"{baseURL}{endpoint}");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            request.AddParameter("application/x-www-form-urlencoded", $"Id={id}&Comments={comments}&Adjuster={adjuster}&Response={isApproved}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
            
        }


        //[Route("TimePeriod/ProcedureSetup/{benefitCodeId}/{planId}/{timePeriodId}/{status}/{procedurecode}")]
        public ActionResult ProcedureSetup(int benefitCodeId, int planId, int timePeriodId, bool status, string procedurecode)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.ProcedureSetup;

            string Adjuster = User.Identity.Name;
            var client = new RestClient($"{baseURL}{endpoint}/{benefitCodeId}/{planId}/{timePeriodId}/{status}/{Adjuster}/{procedurecode}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }
        




        [Route("TimePeriod/GetTimeMonetaryLimits/{planId}/{timePeriodId}")]
        public ActionResult GetTimeMonetaryLimits(int planId, int timePeriodId)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.GetTimeMonetaryLimits;

            var client = new RestClient($"{baseURL}{endpoint}/{planId}/{timePeriodId}");
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        [Route("TimePeriod/SaveTimeMonetaryLimits/{planId}/{timePeriodId}/{amount}")]
        public ActionResult SaveTimeMonetaryLimits(int planId, int timePeriodId, double amount)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.SaveTimeMonetaryLimits;
            string adjuster = User.Identity.Name.Replace("@hygeiahmo.com", "");

            var client = new RestClient($"{baseURL}{endpoint}/{planId}/{timePeriodId}/{amount}/{adjuster}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }

        [Route("TimePeriod/UpdateTimeMonetaryLimits/{id}/{status}")]
        public ActionResult UpdateTimeMonetaryLimits(int id, bool status)
        {
            Token t = new Token();
            t = t.GetToken("admin");

            string baseURL = APIServices.BaseURL;
            string endpoint = APIServices.UpdateTimeMonetaryLimits;

            var client = new RestClient($"{baseURL}{endpoint}/{id}/{status}");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("authorization", "Bearer " + t.Tokn);
            IRestResponse response = client.Execute(request);

            return Json(new { Success = (response.IsSuccessful), Result = response.Content }, JsonRequestBehavior.AllowGet);
        }
    }
}