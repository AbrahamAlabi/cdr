﻿using HyAdmin.AppUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Mvc;

namespace HyDirect.Filters
{
    public class ExceptionHandler : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled || filterContext.HttpContext.IsCustomErrorEnabled)
            {
                return;
            }
            Exception e = filterContext.Exception;
            string controller = filterContext.RouteData.Values["controller"].ToString();
            string action = filterContext.RouteData.Values["action"].ToString();
            string url = filterContext.HttpContext.Request.Url.ToString();
            string IP = Util.GetIpAddres();
            var macAddr = (from nic in NetworkInterface.GetAllNetworkInterfaces() where nic.OperationalStatus == OperationalStatus.Up select nic.GetPhysicalAddress().ToString()).FirstOrDefault();


            Util.LogException(e, action, controller, url, IP, macAddr);
            filterContext.ExceptionHandled = (System.Configuration.ConfigurationManager.AppSettings["env"] == "production");
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"
            };
        }
    }
}