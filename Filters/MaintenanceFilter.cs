﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HyDirect.Filters
{
    public class MaintenanceFilter : ActionFilterAttribute
    {
        //public override void OnResultExecuting(ResultExecutingContext filterContext)
        //{
        //    //You may fetch data from database here 
        //    filterContext.Controller.ViewBag.GreetMesssage = "Hello Foo";
        //    base.OnResultExecuting(filterContext);
        //}

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            #region Hide
            //var controllerName = filterContext.RouteData.Values["controller"];
            //var actionName = filterContext.RouteData.Values["action"];
            //var message = String.Format("{0} controller:{1} action:{2}", "onactionexecuting", controllerName, actionName);
            //Debug.WriteLine(message, "Action Filter Log");
            //base.OnActionExecuting(filterContext); 
            #endregion

            using (var db = new HyAdmin.Models.ExistingDBs.HyDirect())
            {
                DateTime today = DateTime.Now;
                var appSetting = db.AppSettings.Where(a => a.Name == "Maintenance" && a.IsCurrent).FirstOrDefault();
                if (appSetting != null)
                {
                    if (appSetting.StartTime <= today && today < appSetting.EndTime)
                    {
                        filterContext.Controller.ViewBag.EndTime = appSetting.EndTime;
                        filterContext.Result = new ViewResult()
                        {
                            ViewName = "Maintenance"
                        };
                    }
                }
            }
        }
    }

}